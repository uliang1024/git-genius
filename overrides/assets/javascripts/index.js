const observer = new IntersectionObserver((entries) => {
  entries.forEach((entry) => {
    if (entry.isIntersecting) {
      entry.target.classList.add("show");
      observer.unobserve(entry.target);
    }
  });
});

const hiddenElements = document.querySelectorAll(
  ".bounce-left, .bounce-right, .bounce-top, .bounce-bottom, .bounce-scale, .bounce-top-left, .bounce-top-right, .bounce-bottom-left, .bounce-bottom-right"
);

hiddenElements.forEach((el) => observer.observe(el));

document.addEventListener("transitionend", (event) => {
  if (event.target.classList.contains("show")) {
    observer.unobserve(event.target);
  }
});

$(function () {
  var data = [
    {
      action: "type",
      strings: ["git --version^400"],
      output: '<span class="gray">git version 2.40.0.1</span><br>&nbsp;',
      postDelay: 1000,
    },
    {
      action: "type",
      strings: ["git -v^400"],
      output: '<span class="gray">git version 2.40.0.1</span><br>&nbsp;',
      postDelay: 1000,
    },
    {
      action: "type",
      strings: ["list_git_learning_resources^400"],
      output: $(".mimik-run-output").html(),
    },
    {
      action: "type",
      strings: ["that was easy!", ""],
      postDelay: 2000,
    },
  ];
  runScripts(data, 0);
});

function runScripts(data, pos) {
  var prompt = $(".prompt"),
    script = data[pos];
  if (script.clear === true) {
    $(".history").html("");
  }
  switch (script.action) {
    case "type":
      prompt.removeData();
      $(".typed-cursor").text("");
      prompt.typed({
        strings: script.strings,
        typeSpeed: 30,
        callback: function () {
          var history = $(".history").html();
          history = history ? [history] : [];
          history.push("$ " + prompt.text());
          if (script.output) {
            history.push(script.output);
            prompt.html("");
            $(".history").html(history.join("<br>"));
          }
          $("section.terminal").scrollTop($("section.terminal").height());
          pos++;
          if (pos < data.length) {
            setTimeout(function () {
              runScripts(data, pos);
            }, script.postDelay || 1000);
          }
        },
      });
      break;
    case "view":
      break;
  }
}

const goElements = document.querySelectorAll(".go");

function checkVisibility() {
  goElements.forEach(function (element) {
    const rect = element.getBoundingClientRect();
    if (rect.top < window.innerHeight && rect.bottom >= 0) {
      element.classList.remove("hidden", "z-n1");
    }
  });
}

window.addEventListener("scroll", checkVisibility);
checkVisibility();

function playVideo(videos, video) {
  videos.forEach((v) => (v.style.display = "none"));
  video.style.display = "block";
  video.currentTime = 0;
  video.play();
}

function setupSectionClick(sectionId, videoIds, btnIds) {
  const section = document.getElementById(sectionId);
  const videos = videoIds.map((id) => document.getElementById(id));

  btnIds.forEach((id, index) => {
    document.getElementById(id).addEventListener("change", function () {
      if (this.checked) {
        playVideo(videos, videos[index]);
      }
    });
  });

  return function () {
    section.classList.remove("hidden", "z-n1");
  };
}

const repoClick = document.querySelector("#repoClick");
const faDatabases = document.querySelectorAll("#section1 .fa-database.hidden");
const gitClones = document.querySelectorAll(".gitClone");
repoClick.addEventListener("click", () => {
  faDatabases.forEach((db) => db.classList.remove("hidden"));
  gitClones.forEach((gitClone) => gitClone.classList.add("hidden"));
});

const localElements = ["local1", "local2", "local3", "local4"].map((id) =>
  document.getElementById(id)
);
const workingDirectory = document.getElementById("workingDirectory");

document.getElementById("local1").addEventListener("click", function () {
  localElements.forEach((element) => element.classList.add("hidden"));
  repoClick.classList.add("hidden");

  Object.assign(this.style, {
    transition: "all 0.5s ease",
    bottom: "auto",
    left: "50%",
    top: "10%",
    transform: "translate(-50%, -50%)",
    fontSize: "100px",
  });

  workingDirectory.classList.remove("hidden", "z-n1");

  const videos = ["video1-1", "video1-2", "video1-3"].map((id) =>
    document.getElementById(id)
  );

  ["btnradio1-1", "btnradio1-2", "btnradio1-3"].forEach((id, index) => {
    document.getElementById(id).addEventListener("change", function () {
      if (this.checked) {
        playVideo(videos, videos[index]);
      }
    });
  });
});

const sections = [
  {
    id: "goSection2",
    sectionId: "stagingArea",
    videoIds: ["video2-1", "video2-2"],
    btnIds: ["btnradio2-1", "btnradio2-2"],
  },
  {
    id: "goSection3",
    sectionId: "localRepository",
    videoIds: ["video3-1", "video3-2"],
    btnIds: ["btnradio3-1", "btnradio3-2"],
  },
  {
    id: "goSection4",
    sectionId: "gitLog",
    videoIds: ["video4-1", "video4-2"],
    btnIds: ["btnradio4-1", "btnradio4-2"],
  },
];

sections.forEach(({ id, sectionId, videoIds, btnIds }) => {
  document
    .getElementById(id)
    .addEventListener("click", setupSectionClick(sectionId, videoIds, btnIds));
});

const graphContainer = document.getElementById("graph-container");
const gitgraph = GitgraphJS.createGitgraph(graphContainer);
const headArrow = document.getElementById("head-arrow");

let branches = { main: gitgraph.branch("main") };
let currentBranch = branches.main;

branches.main.commit({
  subject: "Initial commit",
  author: "leoChen <2400215@systex.com.tw>",
});

function updateArrowPosition(branch) {
  const commits = branch._branch.gitgraph.commits;
  const lastCommit = commits
    .slice()
    .reverse()
    .find((commit) => commit.branches.includes(branch.name));

  if (lastCommit) {
    const lastCommitElement = document.getElementById(lastCommit.hash);
    if (lastCommitElement) {
      const rect = lastCommitElement.getBoundingClientRect();
      const graphContainerRect = graphContainer.getBoundingClientRect();

      headArrow.style.left = `${
        rect.left + rect.width / 2 - graphContainerRect.left
      }px`;
      headArrow.style.top = `${rect.top - graphContainerRect.top - 20}px`;
      headArrow.style.display = "block";
    }
  }
}

const gitCommands = [
  "git commit -m",
  "git checkout",
  "git checkout -b",
  "git branch",
  "git branch -d",
  "git merge",
];

$("#git-command").autocomplete({ source: gitCommands });

$("#git-command").on("keydown", function (event) {
  if (event.key === "Tab") {
    event.preventDefault();
    const firstSuggestion = $(".ui-menu-item").first().text();
    $(this).val(firstSuggestion);
  }
});

function handleCommand(command) {
  const trimCommand = command.trim();
  if (trimCommand.startsWith("git commit -m ")) {
    const message = trimCommand.slice(15, -1);
    currentBranch.commit({
      subject: message,
      author: "leoChen <2400215@systex.com.tw>",
    });
    setTimeout(() => updateArrowPosition(currentBranch), 100);
  } else if (trimCommand.startsWith("git checkout ")) {
    const branchCommand = trimCommand.slice(13).trim();
    handleCheckout(branchCommand);
  } else if (trimCommand.startsWith("git branch ")) {
    const branchCommand = trimCommand.slice(11).trim();
    handleBranch(branchCommand);
  } else if (trimCommand.startsWith("git merge ")) {
    const branchName = trimCommand.slice(10).trim();
    handleMerge(branchName);
  } else {
    alert("Invalid command.");
  }
}

function handleCheckout(branchCommand) {
  if (branchCommand.startsWith("-b ")) {
    const branchName = branchCommand.slice(3).trim();
    if (!branches[branchName]) {
      branches[branchName] = gitgraph.branch(branchName);
      currentBranch = branches[branchName];
      setTimeout(() => updateArrowPosition(currentBranch), 100);
    } else {
      alert(`Branch "${branchName}" already exists.`);
    }
  } else {
    const branchName = branchCommand;
    if (branches[branchName]) {
      currentBranch = branches[branchName];
      setTimeout(() => updateArrowPosition(currentBranch), 100);
    } else {
      alert(`Branch "${branchName}" does not exist.`);
    }
  }
}

function handleBranch(branchCommand) {
  if (branchCommand.startsWith("-d ")) {
    const branchName = branchCommand.slice(3).trim();
    if (branches[branchName]) {
      if (
        currentBranch._branch.gitgraph.commits[
          currentBranch._branch.gitgraph.commits.length - 1
        ].branches[0].includes(branchName)
      ) {
        alert(
          `Cannot delete the branch "${branchName}" because it has unmerged changes.`
        );
      } else {
        branches[branchName].delete();
        delete branches[branchName];
      }
    } else {
      alert(`Branch "${branchName}" does not exist.`);
    }
  } else {
    const branchName = branchCommand;
    if (!branches[branchName]) {
      branches[branchName] = gitgraph.branch(branchName);
    } else {
      alert(`Branch "${branchName}" already exists.`);
    }
  }
}

function handleMerge(branchName) {
  if (branchName === currentBranch.name) {
    alert("Cannot merge the current branch into itself.");
    return;
  }
  const targetBranch = branches[branchName];
  if (targetBranch) {
    currentBranch.merge(targetBranch, { fastForward: true });
    setTimeout(() => updateArrowPosition(currentBranch), 100);
  } else {
    alert(`Branch "${branchName}" does not exist.`);
  }
}

document
  .getElementById("git-command")
  .addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
      handleCommand(event.target.value);
      event.target.value = "";
    }
  });

setTimeout(() => updateArrowPosition(currentBranch), 100);
