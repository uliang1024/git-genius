# **Local 到 Remote**

---

## **git clone**

從遠端倉庫克隆一份完整的 Git 倉庫到本地。複製所有的歷史記錄、分支和文件，以便在本地進行開發或操作。

=== "HTTPS"

    ``` sh title="Clone with HTTPS"
    git clone https://gitlab.com/uliang1024/git-genius.git
    ```

    <div class="result" markdown>

    ![alt text](<../assets/git clone-1.png>){ align=left width=300 }

    使用HTTPS來複製URL是比較簡單的。但是想要抓取(fetch)或推到(push)遠端時，就需要在輸入帳號密碼，如果不需要大多是帳密(Key Chain)已存在電腦內。

    </div>

=== "SSH"

    ``` sh title="Clone with SSH"
    git clone git@gitlab.com:uliang1024/git-genius.git
    ```

    <div class="result" markdown>

    ![alt text](<../assets/git clone-2.png>){ align=right width=300 }

    使用SSH來複製URL就需要先設置 SSH key(金鑰)。雖然需要先在電腦內設定好金鑰，但再上傳時就不需要再輸入額外帳密。在多人合作時，就會很方便。

    </div>

## **git remote**

git remote指令用於管理Git 倉庫中的遠端倉庫。git remote指令提供了一些用於查看、新增、重新命名和刪除遠端倉庫的功能。  

**以下是git remote 指令的常見用法**：  

- git remote：列出目前倉庫中已設定的遠端倉庫。
- git remote -v：列出目前倉庫中已配置的遠端倉庫，並顯示它們的URL。
- git remote add <remote_name> <remote_url>：新增一個新的遠端倉庫。指定一個遠端倉庫的名稱和URL，將其新增至目前倉庫。
- git remote rename <old_name> <new_name>：將已設定的遠端倉庫重新命名。
- git remote remove <remote_name>：從目前倉庫中刪除指定的遠端倉庫。
- git remote set-url <remote_name> <new_url>：修改指定遠端倉庫的URL。
- git remote show <remote_name>：顯示指定遠端倉庫的詳細信息，包括URL 和追蹤分支。

---

## **git fetch**

獲取遠端的修改但又不想將它們合併到您目前的本地分支中

`git fetch` 執行了兩個主要步驟，包含：

- 下載 remote 有的 commit，但是在我們的 local repository 是沒有該 commit。
- 更新我們 remote branch 所指向的地方（例如， o/main）

---

## **git pull**

從遠端倉庫拉取（git fetch）更新並將它們合併(git merge)到當前本地分支。這個命令叫作 `git pull`。

=== "指令"

    ```sh
    git pull <remote_name> <branch_name> #例如 git pull origin main
    ```

=== "等同於"

    ```sh
    git fetch
    git merge <remote_name/branch_name> #例如 git merge o/main
    ```

或者

=== "指令"

    ```sh
    git pull --rebase
    ```

=== "等同於"

    ```sh
    git fetch
    git rebase o/main
    ```

---

## **git push**

將本地提交推送到遠端倉庫。當本地做了**修改**並且希望將這些更改**分享給其他協作者**時。

``` sh title="指令"
git push <remote_name> <branch_name>
```

---

??? question ".gitignore?"

    ## 專案中每個文件都需要push至repo？ 看看甚麼是 [.gitignore](../pages/Lesson06.md)
