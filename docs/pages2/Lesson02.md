# **使用 git stash 暫存**

---

:star: **暫存**<span style="color:red">工作目錄</span>與<span style="color:red">索引</span>的變更狀態

**事件**：當您在進行某項工作時，突然需要切換到其他分支處理<span style="color:red">緊急任務</span>或者<span style="color:red">需要檢查其他分支</span>的內容時

## **建立暫存**

`git stash` 指令，可以自動幫你把改寫到一半的那些檔案建立一個「特殊的版本」，我們稱這些版本為 stash 版本，或你可以直接稱他為「暫存版」。

**用法：**
=== "將已追蹤的檔案建立暫存版"

    ```sh
    git stash 
    ```

=== "全部都建立成暫存版"

    ``` sh
    git stash -u
    ```

`git stash list` 列出目前所有的 stash 清單


![git-stash-list](../assets/git-stash-list.png){width=100%}


## **取回暫存版本**

=== "取回並刪除stash分支"
    ```sh
    git stash pop
    ```
=== "取回但不刪除stash分支"
    ```sh
    git stash apply
    ```

## **建立多重暫存版**

**事件**：上一個緊急任務還沒結束又來更緊急的任務

```sh title="自訂暫存版的註解"
git stash save -u <message> 
```

![git-stash-list](../assets/git-stash-list2.png){width=100%}

直接執行 `git stash apply` 的話，他會取回最近的一筆暫存版，也就是上述例子的 `stash@{0}`

如果想取回「特定一個暫存版」，必須在最後指名 stash id，如下範例：
```sh
git stash apply "stash@{1}"
```

- 刪除 stash@{1} 暫存版，可以透過 `git stash drop "stash@{1}"` 將特定暫存版刪除。

- 清理掉所有的暫存版，直接下達 `git stash clear` 即可全部刪除。
