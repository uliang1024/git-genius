# **取消文件修改的 Git 操作練習**

---

以下流程主要是在演示如何使用 Git 來<span style="color:red">取消暫存</span>和<span style="color:red">恢復文件</span>的操作。

```mermaid
    graph TD
        A[選擇文件並進行修改] --修改文件--> A1[git add 將文件添加到暫存區]
        A1 --2.git add file--> B[運行 git status 檢查文件狀態]
        B --3. 查看文件狀態--> C[是否需要取消暫存文件]
        C -- Yes --> D[運行 git restore --staged file 取消暫存]
        D --4. 取消暫存文件--> E[再次運行 git status 檢查文件狀態]
        E --5. 檢查文件狀態--> F[是否需要恢復文件]
        F -- Yes--> G[運行 git restore file 恢復文件]
        G --6. 恢復文件--> H[再次運行 git status 檢查文件狀態]
        H --7. 確認文件恢復--> I[操作完成]
        F -- No --> I
        C -- No --> I
```
