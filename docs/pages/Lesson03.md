# **git status, git log**

---

```mermaid
graph LR
    WD[Working Directory<br/>工作目錄] -- git add --> SA[Staging Area<br/>暫存區]
    SA -- git commit --> LR[Local Repository<br/>本地儲存庫]
```

## **文件狀態檢查（git status）**

<span style="color:red">檢查當前目錄的狀態，查看哪些更改已暫存、哪些未暫存。</span>

---

```sh title="語法"
git status
```

[執行腳本](javascript:void(0);){ .md-button #codeBtn1 } 輸出狀態 —> <span style="color:red">未追蹤 (Untracked files)</span>

<div class="terminal-window" id="git-status1">
    <header>
        <div class="button green"></div>
        <div class="button yellow"></div>
        <div class="button red"></div>
    </header>
    <section class="terminal">
        <div class="history"></div> $&nbsp;<span class="prompt"></span>
        <span class="typed-cursor"></span>
    </section>
</div>

---

我們建立新的檔案，或是更改檔案時，都會執行 git add指令將檔案加至暫存區（Staging Area）

```sh
git add
```

[執行腳本](javascript:void(0);){ .md-button #codeBtn2 } 輸出狀態 —> <span style="color:red">等待提交(Changes to be committed)</span>

<div class="terminal-window" id="git-status2">
    <header>
        <div class="button green"></div>
        <div class="button yellow"></div>
        <div class="button red"></div>
    </header>
    <section class="terminal">
        <div class="history"></div> $&nbsp;<span class="prompt"></span>
        <span class="typed-cursor"></span>
    </section>
</div>

---

我們再次改變 `Lesson03.md` 內容，並查看git status 的狀態

[執行腳本](javascript:void(0);){ .md-button #codeBtn3 } 輸出狀態  —> <span style="color:red">已更改 (Changes not staged for commit)</span>

<div class="terminal-window" id="git-status3">
    <header>
        <div class="button green"></div>
        <div class="button yellow"></div>
        <div class="button red"></div>
    </header>
    <section class="terminal">
        <div class="history"></div> $&nbsp;<span class="prompt"></span>
        <span class="typed-cursor"></span>
    </section>
</div>

將暫存區的檔案提交到儲存庫永久儲存

``` sh
git commit -m "提交訊息"
```

查看現在文件狀態

[執行腳本](javascript:void(0);){ .md-button #codeBtn4 } 輸出狀態 —> <span style="color:red">無任何提交資訊，已提交(Committed)</span>

<div class="terminal-window" id="git-status4">
    <header>
        <div class="button green"></div>
        <div class="button yellow"></div>
        <div class="button red"></div>
    </header>
    <section class="terminal">
        <div class="history"></div> $&nbsp;<span class="prompt"></span>
        <span class="typed-cursor"></span>
    </section>
</div>

## **查看提交歷史 (git log)**

`git log` 會列出提交歷史，包括每次提交的作者、日期、提交消息等信息。

```sh
git log
```

[執行腳本](javascript:void(0);){ .md-button #codeBtn5 }

<div class="terminal-window" id="git-status5">
    <header>
        <div class="button green"></div>
        <div class="button yellow"></div>
        <div class="button red"></div>
    </header>
    <section class="terminal">
        <div class="history"></div> $&nbsp;<span class="prompt"></span>
        <span class="typed-cursor"></span>
    </section>
</div>
