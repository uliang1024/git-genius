# **使用Tag標籤 git tag**

---

```mermaid
%%{init: { 'logLevel': 'debug', 'theme': 'default' , 'themeVariables': {
              'tagLabelFontSize': '16px'
       } } }%%
    gitGraph
        commit tag: "v1.0.0"
        branch develope
        commit
        checkout main
        merge develope tag: "v2.0.0"
```

`git tag` 是用來在 Git 中標記特定的提交（commit）的工具。它可以用來標記發布版本、重要的里程碑或任何您認為有意義的提交。標記通常用於標識軟體版本號或重要的發布日期。

例如：

`1.1.4`

`NNN.abc.xxx`

- `NNN`: 大版本號
- `abc`: 每次做出小更新時，發布的版本號
- `xxx`: 每次bug修正時發佈的版本號

你可以使用 git tag 命令來創建、列出、刪除和顯示標記。以下是一些常見的 git tag 使用案例：

- 創建標籤： 使用 `git tag <tag_name>` 創建一個標籤，例如 git tag v1.0.0。
- 列出標籤： 使用 `git tag` 命令列出所有的標籤。
- 顯示標籤信息： 使用 `git show <tag_name>` 來顯示特定標籤的詳細信息。
- 刪除標籤： 使用 `git tag -d <tag_name>` 來刪除指定的標籤。

標籤通常用於管理軟體版本和發布。當您準備好發布新版本時，您可以使用標籤來標識這個特定版本的提交。
