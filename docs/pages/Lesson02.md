# **Git 初始設定**

---

## **建立一個Git庫**

```sh title="初始化"
git init
```

初始化一個全新的 Git 儲存庫（repository），追蹤和管理專案的版本控制歷史。

## **列出 Git 的配置信息**

``` sh
git config --list
```

> 更多config資訊： [https://git-scm.com/docs/git-config/en](https://git-scm.com/docs/git-config/en)
