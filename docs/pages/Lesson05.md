# **比較修改內容 git diff**

---

`git diff`用於<span style="color:red">比較文件之間的差異</span>。這個命令通常用於檢查修改的內容，並確定要提交的更改。

常見用法：

比較**工作目錄**和**暫存區**
   ```
   git diff
   ```

![alt text](../assets/diff1.png)

比較**暫存區**和**最後一次提交**
   ```
   git diff --cached
   ```

![alt text](../assets/diff2.png)

比較**工作目錄**和**最後一次提交**
   ```
   git diff HEAD
   ```

![alt text](../assets/diff3.png)

比較**兩個特定提交之間**
   ```
   git diff <commit1> <commit2>
   ```

![alt text](../assets/diff4.png)