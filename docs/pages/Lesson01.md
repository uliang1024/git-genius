# **理解版本管理的流程**

---

## **Git 作業流程**

```mermaid
    sequenceDiagram
    box Local
    participant WD as Working Directory<br/>(工作目錄)
    participant SA as Staging Area<br/>(暫存區)
    participant LR as Local Repository<br/>(本地儲存庫)
    end
    box Remote
    participant RR as Remote Repository<br/>(遠端儲存庫)
    end
    RR-->>WD: git clone <repository_url>
    WD->>SA: git add
    SA->>LR: git commit
    LR->>RR: git push
    RR-->>LR: git fetch
    RR-->>WD: git pull
    LR-->>WD: git checkout <branch_name>
    LR-->>WD: git merge <branch_name>

```

1. **Working Directory (工作目錄)：** 是指包含您當前工作項目的目錄。當您在編輯文件、新增、修改或刪除文件時，這些更改都發生在工作目錄中。
    - 當您執行 `git add` 指令時，將會將工作目錄中的更改添加到暫存區。
2. **Staging Area (暫存區)：** 是一個暫存文件更改的區域，用於準備將要提交的更改。
    - 在暫存區中的更改可以通過 `git commit` 指令提交到本地儲存庫。
3. **Local Repository (本地儲存庫)：** 是指包含您項目完整歷史記錄的本地數據庫。
    - 本地儲存庫保存了您所有提交的快照，您可以通過 `git log` 查看提交歷史記錄。
    - 當您準備將更改從本地提交到遠端時，您可以使用 `git push` 將本地儲存庫中的提交推送到遠端儲存庫。
4. **Remote Repository (遠端儲存庫)：** 是指位於遠端的服務器上的數據庫，它與本地儲存庫相對應。
    - 您可以使用 `git pull` 從遠端儲存庫拉取最新的更改到本地儲存庫。這等同於運行 `git fetch` 來取回遠端儲存庫中的所有分支，然後再執行 `git merge` 來合併所取回的分支到您的當前分支。
    - 初次取得遠端儲存庫中的所有內容通常是通過 `git clone` 來完成的。這個操作會將遠端儲存庫克隆到本地，包括所有歷史記錄和分支。
