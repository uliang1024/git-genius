# **.gitignore Documentation**

---

用於指定 Git **忽略某些文件或目錄的配置文件**。當 Git 檢查文件更改時，Git 將忽略它們，不會將它們包含在版本控制中。

---

![alt text](../assets/gitignore.png)

> 查看更多介紹：[https://git-scm.com/docs/gitignore](https://git-scm.com/docs/gitignore){target="_blank"}