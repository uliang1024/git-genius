# **更新最後的提交 阿們**

---

`git commit --amend` 修改最後一次提交的命令。

使用情況：

- **修改最後一次提交的提交訊息**

```sh
git commit --amend -m "New and improved commit message"
```

- **添加新的更改**：修改最後一次提交內容，而無需創建一個新的提交。

```sh
# 將忘記的更改加入暫存區
git add forgot.changes

# 使用 --amend 參數將新增的更改添加到最後一次提交中
git commit --amend
```

!!! warning "請注意，當您使用 git commit --amend 時，它將修改最後一次提交的內容。"

    因此，如果您的提交<span style="color:red">已經被推送到遠端儲存庫</span>，並且其他人已經基於該提交進行了工作，則修改最後一次提交可能會導致歷史的重新編寫，這可能會<span style="color:red">對其他人的工作產生影響</span>。因此，請在確定沒有其他人基於您的提交進行工作時使用此命令。
