# **返回過去版本 git reset, git revert**

---

 `git reset` 指令時，--soft、--mixed 和 --hard 參數可以控制重置的範圍以及對暫存區和工作目錄的影響程度。

- 影響程度：  

|       選項        |   歷史   |  暫存區  | 工作目錄 |
| :--------------- | :------: | :------: | :------: |
|     `--soft`      | &#10004; |          |          |
| `--mixed`（預設） | &#10004; | &#10004; |          |
|     `--hard`      | &#10004; | &#10004; | &#10004; |

## **具體操作：**  
回復到上一個（或更前的）版本。

=== "回復到最新提交版本"
    ```
    git reset HEAD
    ```
=== "等於 ~1 回復到上一個提交版本"
    ```
    git reset HEAD~
    ```
=== "n 等於往上第幾個提交版本 回復之前指定的提交版本"
    ```
    git reset HEAD~n
    ```

!!! note "小知識"

    HEAD 是什麼 ? HEAD 是一個指標，通常指向某一個分支，通常你可以把 HEAD 當做「目前所在分支」看待。

我們也可以用以下方法，讓HEAD 從 branch中分離出來

=== "指定hash值"

    ```sh
    git checkout <commit_hash> # git log 查詢hash值
    ```
=== "使用 ^ "

    ```sh
    git checkout HEAD^^ # HEAD向上移動2個 commit
    ```
=== "使用 ~ "

    ```sh
    git checkout HEAD~n # HEAD向上移動n個 commit
    ```

---

## **revert**

``` sh
git revert <commit_hash>
```

<div class="result" markdown>

![git revert](../assets/revert1.png){ align=left width=300 }
假設你正在一個共享的專案中工作，並且你發現之前的一個**提交引入了一個錯誤**，但是你**不想改變歷史紀錄**，
因為**其他人可能已經基於該提交進行了進一步的工作**。  

在這種情況下，你可以使用 `git revert`。

</div>

---

## **假設你回覆之前的版本後，又想回復到最新提交的版本，就可以用：**

<div class="grid cards" markdown>

-   **reset** 

    === "返回"

        ```sh
        git reset C1
        ```

        ---

        ![新增分支並commit](../assets/reset1.png){width=100%}

    === "再返回"

        ```sh
        git reset C3
        ```

        ---

        ![新增分支並commit](../assets/reset2.png){width=100%}

-   **revert** 

    === "返回"

        ```sh
        git revert C3
        ```

        ---

        ![新增一段文字](../assets/revert1.png){width=100%}

    === "再返回"

        ```sh
        git revert C4
        ```

        ---

        ![新增一段文字](../assets/revert2.png){width=100%}   

</div>

!!! note "貼心提示"

    如果一開始沒有記下來 Commit 的 hash id 值也沒關係，Git 裡有個 `reflog` 指令有保留一些紀錄。
    當 HEAD 有移動的時候（例如切換分支或是 reset 都會造成 HEAD 移動），Git 就會在 Reflog 裡記上一筆。
    > 小提示  
    > `git log -g` 也可以看到 Reflog 喔！