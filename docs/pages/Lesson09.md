# **branch 分支**

---

```mermaid
    %%{init: { 'logLevel': 'debug', 'theme': 'default' , 'themeVariables': {
            'git0': '#00ff00','gitBranchLabel0': '#ffffff',
            'git1': '#ff0000','gitBranchLabel1': '#ffffff',
            'git2': '#ffff00','gitBranchLabel2': '#000000',
            'git3': '#0000ff','gitBranchLabel3': '#ffffff',
            'git4': '#ff00ff','gitBranchLabel4': '#ffffff',
            'git5': '#ff00ff','gitBranchLabel5': '#ffffff',
            'git6': '#ff00ff','gitBranchLabel6': '#ffffff',
            'git7': '#00ffff','gitBranchLabel7': '#ffffff'
    } } }%%
    gitGraph TB:
        commit tag: "v1.0.0"
        branch develop order: 3
        commit
        branch feature order: 4
        commit
        branch feature2 order: 5
        commit
        checkout feature
        commit
        checkout develop
        commit
        branch release order: 2
        commit
        commit
        checkout develop
        merge feature
        merge release
        merge feature2
        checkout main
        merge release tag: "v2.0.0"
        branch hotfix order: 1
        commit
        checkout develop
        merge hotfix
        checkout main
        merge hotfix tag: "v2.0.1"
```

## **分支（branch）**

- 一條獨立的開發線。
- 在處理新功能或bug 修補時，創建新的分支來獨自工作。
- 分支不會相互干擾。

### **常用的分支指令**

=== "創建一個新分支"

    ```sh
    git branch <branch_name>
    ```

=== "創建並切換到新分支"

    ```sh
    git checkout -b <branch_name>
    ```

---

```sh title="切換到不同的分支。"
git checkout <branch_name>
```

---

=== "查看本地所有分支，並標示目前所在分支"

    ```sh
    git branch
    ```

=== "查看遠端分支"

    ```sh
    git branch -r
    ```

=== "查看所有本地和遠端分支"

    ```sh
    git branch -a
    ```

---

```sh title="刪除指定分支"
git branch -d <branch_name>
```

---

**合併分支**

<div class="grid cards" markdown>

-   **merge** 

    ```sh
    git merge <branch_name>
    ```

    ---

    ![新增分支並commit](../assets/git-rebase.png){width=100%}

-   **rebase** 

    ```sh
    git rebase <target_branch>
    ```

    ---

    ![新增一段文字](../assets/git-merge.png){width=100%}

</div>
