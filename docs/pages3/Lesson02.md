# **解決遠端衝突**

---

```sh title="將剛剛合併好的 feature 推至遠端"
git push origin feature
```

![alt text](../assets/conflict3-2.png){width=100%}

![alt text](../assets/conflict3-1.png){width=100%}

---

```sh title="將剛剛合併好的 feature 推至遠端"
git pull origin feature
```

![alt text](../assets/conflict3-3.png){width=100%}

![alt text](../assets/conflict3-4.png){width=100%}

---

```sh title="將剛剛合併好的 feature 推至遠端"
git push origin feature
```

![alt text](../assets/conflict3.png){width=100%}

![alt text](../assets/conflict3-5.png){width=100%}