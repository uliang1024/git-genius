# **本地分支合併衝突**

---

**發生衝突前**：在main分支延伸出兩個分支

<div class="grid cards" markdown>

-   **建立第一個分支，並提交**

    ```
    git checkout -b feature
    git commit -m 'add Lesson01.md'
    ```

    ---

    ![新增分支並commit](../assets/conflict1-0.png){width=100%}


    ---

    mkdocs.yml：
    ![新增分支並commit](../assets/conflict1-2.png){width=100%}

-   **建立第二個分支，並提交**

    ```
    git checkout -b feature2
    git commit -m 'add Lesson02.md'
    ```

    ---

    ![新增一段文字](../assets/conflict1-1.png){width=100%}

    ---

    mkdocs.yml：
    ![新增一段文字](../assets/conflict1-3.png){width=100%}

</div>

---

## **合併**

```sh title="將`feature2`合併到`feature`"
git checkout feature
git merge feature2
```

<div class="result" markdown>

!!! failure "Merge conflict"
    <figure markdown="span">
    ![alt text](../assets/conflict2.png){width=100%}
    </figure>
</div>

```sh title="觀察衝突的地方"
git status
```

<div class="result" markdown>
<figure markdown="span">
![alt text](../assets/conflict2-2.png){width=100%}
</figure>
</div>

## **查看衝突檔案**

<figure markdown="span">
![alt text](../assets/conflict2-1.png){width=100%}
</figure>

## **解決衝突檔案**

<figure markdown="span">
![alt text](../assets/conflict2-3.png){width=100%}
</figure>

``` sh
git add .
git commit
```

## **成功解決**

<figure markdown="span">
![alt text](../assets/conflict2-4.png){width=100%}
</figure>
