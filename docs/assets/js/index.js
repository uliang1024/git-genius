$("#codeBtn1").click(function () {
  var data = [
    {
      action: "type",
      strings: ["git status^400"],
      output:
        '<br>On branch main<br>Untracked files: <br>&nbsp; (use "git add <file>..." to include in what will be committed) <br>&nbsp; &nbsp; &nbsp; <span class="red">docs/pages/index.md</span> <br> <br> nothing added to commit but untracked files present (use "git add" to track)<br>&nbsp;',
      postDelay: 1000,
    },
  ];
  runScripts(data, 0, "#git-status1");
});

$("#codeBtn2").click(function () {
  var data = [
    {
      action: "type",
      strings: ["git status^400"],
      output:
        "<br>On branch main<br>" +
        "Changes to be committed: <br>" +
        '&nbsp; (use "git restore --staged <file>..." to unstage) <br>' +
        "&nbsp; &nbsp; &nbsp; <span class='green'>new file:   docs/pages/index.md</span> <br>",
      postDelay: 1000,
    },
  ];
  runScripts(data, 0, "#git-status2");
});

$("#codeBtn3").click(function () {
  var data = [
    {
      action: "type",
      strings: ["git status^400"],
      output:
        "<br>On branch main<br>" +
        "Changes to be committed: <br>" +
        '&nbsp; (use "git restore --staged <file>..." to unstage) <br>' +
        "&nbsp; &nbsp; &nbsp; <span class='green'>new file:   docs/pages/index.md</span> <br>" +
        "<br>Changes not staged for commit: <br>" +
        '&nbsp; (use "git add <file>..." to update what will be committed) <br>' +
        '&nbsp; (use "git restore <file>..." to discard changes in working directory) <br>' +
        "&nbsp; &nbsp; &nbsp; <span class='red'>modified:   docs/pages/index.md</span> <br>",
      postDelay: 1000,
    },
  ];
  runScripts(data, 0, "#git-status3");
});

$("#codeBtn4").click(function () {
  var data = [
    {
      action: "type",
      strings: ["git status^400"],
      output: "<br>On branch main<br>nothing to commit, working tree clean<br>",
      postDelay: 1000,
    },
  ];
  runScripts(data, 0, "#git-status4");
});

$("#codeBtn5").click(function () {
  var data = [
    {
      action: "type",
      strings: ["git log^400"],
      output:
        "<br><span class='yellow'>commit 0070bbfa133b6ffdfaa2368174416e1c39750124 (</span></span><span class='blue'>HEAD -> </span><span class='green'>main</span><span class='yellow'>)</span><br>" +
        "Author: uliang1024 <amisleo000@gmail.com><br>" +
        "Date:   Mon Mar 25 10:57:36 2024 +0800<br>" +
        "<br>&nbsp;&nbsp;created index.md<br>",

      postDelay: 1000,
    },
    {
      action: "type",
      strings: ["git log --oneline -5^400"],
      output:
        "<br><span class='yellow'>756fb47 (</span><span class='blue'>HEAD -></span> <span class='green'>main</span>, <span class='red'>origin/main</span>, <span class='red'>origin/fix-plugins</span>, <span class='red'>origin/HEAD</span>) Merge branch 'fix-plugins' into 'main'<br>" +
        "<span class='yellow'>1d2d515</span> 修復套件問題<br>" +
        "<span class='yellow'>7cc5d3d</span> Merge branch 'feature' into 'main'<br>" +
        "<span class='yellow'>2cd1fad (</span><span class='red'>origin/feature</span><span class='yellow'>)</span> abc<br>" +
        "<span class='yellow'>dd22cde</span> Merge branch 'fix-index' into 'main'<br>",

      postDelay: 1000,
    },
  ];
  runScripts(data, 0, "#git-status5");
});

function runScripts(data, pos, terminalId) {
  if (pos == 0) {
    $(terminalId + " .history").html("");
  }
  var prompt = $(terminalId + " .prompt"),
    script = data[pos];
  switch (script.action) {
    case "type":
      // cleanup for next execution
      prompt.removeData();
      $(terminalId + " .typed-cursor").text("");
      prompt.typed({
        strings: script.strings,
        typeSpeed: 30,
        callback: function () {
          var history = $(terminalId + " .history").html();
          history = history ? [history] : [];
          history.push("$ " + prompt.text());
          if (script.output) {
            history.push(script.output);
            prompt.html("");
            $(terminalId + " .history").html(history.join("<br>"));
          }
          // scroll to bottom of screen
          $(terminalId + " section.terminal").scrollTop(
            $(terminalId + " section.terminal").height()
          );
          // Run next script
          pos++;
          if (pos < data.length) {
            setTimeout(function () {
              runScripts(data, pos, terminalId);
            }, script.postDelay || 1000);
          }
        },
      });
      break;
    case "view":
      break;
  }
}

window.onload = function () {
  if (window.location.href.indexOf("/print_page") > -1) {
    window.print();
  }
};
